    đ          2019.4.8f1 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙    Ŕ           1  1  ˙˙˙˙                Ţ                        j  ˙˙˙˙                \     ˙˙˙˙                H r   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                      Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                   \       ŕyŻ     `   
                                                                                                                                                                                ŕyŻ                                                                                 	   GameModel   ,
  using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// The two enums group tile types by their respective layers
public enum BackgroundMapTileType {
    Wall,
    Floor,
    Lava,
}

public enum ForegroundMapTileType {
    Player,
    Exit,
    Collectible,
}

/// <summary>
/// Defines game configurations, holds data only and should be a singleton instance
/// </summary>
[CreateAssetMenu(menuName = "Banditos/Grid Model", fileName = "GridModel")]
public class GameModel : ScriptableObject
{
    [Header("Map configurations")]
    [SerializeField] int gridWidth;
    [SerializeField] int gridHeight;
    [Tooltip("Tile has same width and height")]
    [SerializeField] float tileSize;
    [SerializeField] int minWallObstacleCount;
    [SerializeField] int maxWallObstacleCount;
    [SerializeField] int minLavaObstacleCount;
    [SerializeField] int maxLavaObstacleCount;

    [Header("Visuals")]
    [SerializeField] Sprite playerIcon;
    [SerializeField] Sprite targetIcon;
    [SerializeField] Sprite wallIcon;
    [SerializeField] Sprite floorIcon;
    [SerializeField] Sprite lavaIcon;
    [SerializeField] Sprite collectibleIcon;

    public int GridWidth => gridWidth;

    public int GridHeight => gridHeight;

    public float TileSize => tileSize;

    public Dictionary<BackgroundMapTileType, Sprite> TileIcons => tileIcons;

    public Dictionary<ForegroundMapTileType, Sprite> InteractableIcons => interactableIcons;

    public int CollectiblesCollected { get; set; }

    public int StepsCount { get; set; }

    private Dictionary<BackgroundMapTileType, Sprite> tileIcons;
    private Dictionary<ForegroundMapTileType, Sprite> interactableIcons;

    public void InitializeModel() {
        tileIcons = new Dictionary<BackgroundMapTileType, Sprite>();
        tileIcons.Add(BackgroundMapTileType.Wall, wallIcon);
        tileIcons.Add(BackgroundMapTileType.Floor, floorIcon);
        tileIcons.Add(BackgroundMapTileType.Lava, lavaIcon);
        interactableIcons = new Dictionary<ForegroundMapTileType, Sprite>();
        interactableIcons.Add(ForegroundMapTileType.Player, playerIcon);
        interactableIcons.Add(ForegroundMapTileType.Exit, targetIcon);
        interactableIcons.Add(ForegroundMapTileType.Collectible, collectibleIcon);
        CollectiblesCollected = 0;
        StepsCount = 0;
    }

    public int GetWallObstacleCount() {
        return Random.Range(minWallObstacleCount, maxWallObstacleCount + 1);
    }

    public int GetLavaObstacleCount() {
        return Random.Range(minLavaObstacleCount, maxLavaObstacleCount + 1);
    }
}
                    	   GameModel       