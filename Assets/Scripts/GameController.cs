﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Linq;

public enum MovementDirection {
    Up, Down, Left, Right, None,
}

/// <summary>
/// Handles the game flow, entry and exit points of the game
/// Detects input, moves player and handles the outcome
/// </summary>
public class GameController : MonoBehaviour
{
    [SerializeField] GameModel gameModel;
    [SerializeField] GridView gridView;
    [SerializeField] UIController uiController;

    public ForegroundMapTile PlayerInstance { get; set; }
    public ForegroundMapTile TargetInstance { get; set; }
    public List<ForegroundMapTile> CollectibleInstances { get; private set; }

    private MovementDirection currentMovementDirection;
    private bool didFinishGame;

    // Init everything in Awake
    protected void Awake() {
        CollectibleInstances = new List<ForegroundMapTile>();
        gameModel.InitializeModel();
        gridView.GenerateMap();
        didFinishGame = false;
    }

    // Show UI after game has been initialized
    protected void Start() {
        uiController.RefreshUI();
    }

    protected void Update() {
        if (didFinishGame) return;

        if (Input.GetKeyDown(KeyCode.UpArrow)) {
            currentMovementDirection = MovementDirection.Up;
        } else if (Input.GetKeyDown(KeyCode.DownArrow)) {
            currentMovementDirection = MovementDirection.Down;
        } else if (Input.GetKeyDown(KeyCode.LeftArrow)) {
            currentMovementDirection = MovementDirection.Left;
        } else if (Input.GetKeyDown(KeyCode.RightArrow)) {
            currentMovementDirection = MovementDirection.Right;
        } else {
            currentMovementDirection = MovementDirection.None;
        }

        HandleMovement();
    }

    private void HandleMovement() {
        if (currentMovementDirection == MovementDirection.None) return;
        var nextIndex = GetNextIndex();

        // If wall, don't move
        if (gridView.Map[nextIndex].BackgroundMapTileType == BackgroundMapTileType.Wall) {
            return;
        }
        // Move player and document
        PlayerInstance.SetIndex(nextIndex);
        gameModel.StepsCount++;
        uiController.RefreshUI();

        // Handle win/lose/star
        if (TargetInstance.GridIndex == nextIndex) {
            didFinishGame = true;
            uiController.ShowFinishMenu(true);
        } else if (gridView.Map[nextIndex].BackgroundMapTileType == BackgroundMapTileType.Lava) {
            didFinishGame = true;
            uiController.ShowFinishMenu(false);
        }
        var collectibleIndices = CollectibleInstances.Select(tile => tile.GridIndex).ToList();
        if (collectibleIndices.Contains(nextIndex)) {
            CollectCollectible(nextIndex);
        }
    }

    void CollectCollectible(Vector2 nextIndex) {
        gameModel.CollectiblesCollected++;
        uiController.RefreshUI();
        var collectible = CollectibleInstances.Find(tile => tile.GridIndex == nextIndex);
        CollectibleInstances.Remove(collectible);
        Destroy(collectible.gameObject);
    }

    private Vector2 GetNextIndex() {
        var nextIndex = PlayerInstance.GridIndex;
        switch (currentMovementDirection) {
            case MovementDirection.Up:
                nextIndex += Vector2.up;
                break;
            case MovementDirection.Down:
                nextIndex += Vector2.down;
                break;
            case MovementDirection.Left:
                nextIndex += Vector2.left;
                break;
            case MovementDirection.Right:
                nextIndex += Vector2.right;
                break;
        }

        return nextIndex;
    }

    public void Button_RestartGame() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
