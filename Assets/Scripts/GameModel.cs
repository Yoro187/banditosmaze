﻿using UnityEngine;
using System.Collections.Generic;

// The two enums group tile types by their respective layers
public enum BackgroundMapTileType {
    Wall,
    Floor,
    Lava,
}

public enum ForegroundMapTileType {
    Player,
    Exit,
    Collectible,
}

/// <summary>
/// Defines game configurations, holds data only and should be a singleton instance
/// </summary>
[CreateAssetMenu(menuName = "Banditos/Grid Model", fileName = "GridModel")]
public class GameModel : ScriptableObject
{
    [Header("Map configurations")]
    [SerializeField] int gridWidth;
    [SerializeField] int gridHeight;
    [Tooltip("Tile has same width and height")]
    [SerializeField] float tileSize;
    [SerializeField] int minWallObstacleCount;
    [SerializeField] int maxWallObstacleCount;
    [SerializeField] int minLavaObstacleCount;
    [SerializeField] int maxLavaObstacleCount;

    [Header("Visuals")]
    [SerializeField] Sprite playerIcon;
    [SerializeField] Sprite targetIcon;
    [SerializeField] Sprite wallIcon;
    [SerializeField] Sprite floorIcon;
    [SerializeField] Sprite lavaIcon;
    [SerializeField] Sprite collectibleIcon;

    public int GridWidth => gridWidth;

    public int GridHeight => gridHeight;

    public float TileSize => tileSize;

    public Dictionary<BackgroundMapTileType, Sprite> TileIcons => tileIcons;

    public Dictionary<ForegroundMapTileType, Sprite> InteractableIcons => interactableIcons;

    public int CollectiblesCollected { get; set; }

    public int StepsCount { get; set; }

    private Dictionary<BackgroundMapTileType, Sprite> tileIcons;
    private Dictionary<ForegroundMapTileType, Sprite> interactableIcons;

    public void InitializeModel() {
        tileIcons = new Dictionary<BackgroundMapTileType, Sprite>();
        tileIcons.Add(BackgroundMapTileType.Wall, wallIcon);
        tileIcons.Add(BackgroundMapTileType.Floor, floorIcon);
        tileIcons.Add(BackgroundMapTileType.Lava, lavaIcon);
        interactableIcons = new Dictionary<ForegroundMapTileType, Sprite>();
        interactableIcons.Add(ForegroundMapTileType.Player, playerIcon);
        interactableIcons.Add(ForegroundMapTileType.Exit, targetIcon);
        interactableIcons.Add(ForegroundMapTileType.Collectible, collectibleIcon);
        CollectiblesCollected = 0;
        StepsCount = 0;
    }

    public int GetWallObstacleCount() {
        return Random.Range(minWallObstacleCount, maxWallObstacleCount + 1);
    }

    public int GetLavaObstacleCount() {
        return Random.Range(minLavaObstacleCount, maxLavaObstacleCount + 1);
    }
}
