﻿using UnityEngine;

public class BackgroundMapTile : MonoBehaviour
{
    [SerializeField] SpriteRenderer tileSpriteRenderer;
    [SerializeField] GameModel gameModel;

    public BackgroundMapTileType BackgroundMapTileType => backgroundMapTileType;

    public Vector2 TileIndex => tileIndex;

    private BackgroundMapTileType backgroundMapTileType;
    private Vector2 tileIndex;

    public void InitializeTile(BackgroundMapTileType backgroundMapTileType, Vector2 index) {
        name = backgroundMapTileType.ToString();
        this.backgroundMapTileType = backgroundMapTileType;
        tileSpriteRenderer.sprite = gameModel.TileIcons[backgroundMapTileType];
        tileIndex = index;
    }
}
