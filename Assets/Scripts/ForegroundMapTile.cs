﻿using UnityEngine;

/// <summary>
/// Represents a foreground sprite
/// Can be placed on the same index as background tile and moves if player
/// Thought about creating a MovableMapTile : ForegroundMapTile but thought it was overkill for the design
/// </summary>
public class ForegroundMapTile : MonoBehaviour
{
    [SerializeField] SpriteRenderer tileSpriteRenderer;
    [SerializeField] GameModel gameModel;

    public Vector2 GridIndex {
        get => gridIndex;
    }

    private Vector2 gridIndex;
    private ForegroundMapTileType tileType;

    public void InitializeTile(ForegroundMapTileType tileType, Vector2 index) {
        this.tileType = tileType;
        tileSpriteRenderer.sprite = gameModel.InteractableIcons[tileType];
        gridIndex = index;
    }

    public void SetIndex(Vector2 nextIndex) {
        if (tileType != ForegroundMapTileType.Player) return;
        transform.position = nextIndex;
        gridIndex = nextIndex;
    }
}
