﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles the game's UI
/// Refreshes UI on demand (not concurrently)
/// </summary>
public class UIController : MonoBehaviour
{
    [SerializeField] Text collectiblesCountText;
    [SerializeField] Text stepsCountText;
    [SerializeField] GameObject finishPanel;
    [SerializeField] Text finishPanelTitle;
    [SerializeField] GameModel gameModel;

    protected void Awake() {
        finishPanel.SetActive(false);
    }

    public void RefreshUI() {
        collectiblesCountText.text = "Stars: " + gameModel.CollectiblesCollected;
        stepsCountText.text = "Steps: " + gameModel.StepsCount;
    }

    public void ShowFinishMenu(bool didWin) {
        finishPanelTitle.text = didWin ? "You Win!!!" : "You Lose :(";
        finishPanel.SetActive(true);
    }
}
