﻿using UnityEngine;
using Random = UnityEngine.Random;
using System.Collections.Generic;

/// <summary>
/// Handles map generation on screen. Takes map data from game model.
/// </summary>
public class GridView : MonoBehaviour
{
    [SerializeField] GameModel gameModel;
    [SerializeField] GameController gameController;
    [SerializeField] BackgroundMapTile backgroundMapTilePrefab;
    [SerializeField] ForegroundMapTile playerPrefab;
    [SerializeField] ForegroundMapTile targetPrefab;
    [SerializeField] ForegroundMapTile collectiblePrefab;

    public Dictionary<Vector2, BackgroundMapTile> Map => map;

    private Dictionary<Vector2, BackgroundMapTile> map;

    public void GenerateMap() {
        map = new Dictionary<Vector2, BackgroundMapTile>();
        var currentIndex = Vector2.zero;
        // Generate background map
        for (var i = 0; i < gameModel.GridHeight; i++) {
            for (int j = 0; j < gameModel.GridWidth; j++) {
                var mapTile = Instantiate(backgroundMapTilePrefab, currentIndex, Quaternion.identity, transform);
                var tileType = GetTileType(currentIndex);
                mapTile.InitializeTile(tileType, currentIndex);
                map.Add(currentIndex, mapTile);
                currentIndex.x += gameModel.TileSize;
            }
            currentIndex.y += gameModel.TileSize;
            currentIndex.x = 0;
        }
        // Place player
        var player = SpawnPlayer();
        // Place target
        SpawnTarget(player);
        // Generate a winning path to get indices to avoid when placing obstacles
        var winningPathIndices = GeneratePathToTarget();
        var vacantIndices = GetVacantIndices(winningPathIndices);
        vacantIndices = AddObstacles(vacantIndices);
        SpawnCollectibles(winningPathIndices, vacantIndices);
    }

    // Adds walls and lava according to game model specs
    // Returns updated list of vacant indices for collectibles
    private List<Vector2> AddObstacles(List<Vector2> vacantIndices) {
        var wallObstaclesCount = gameModel.GetWallObstacleCount();
        for (var i = 0; i < wallObstaclesCount; i++) {
            var randomIndex = vacantIndices[Random.Range(0, vacantIndices.Count)];
            var mapTile = map[randomIndex];
            mapTile.InitializeTile(BackgroundMapTileType.Wall, randomIndex);
            vacantIndices.Remove(randomIndex);
        }
        var lavaObstaclesCount = gameModel.GetLavaObstacleCount();
        for (var i = 0; i < lavaObstaclesCount; i++) {
            var randomIndex = vacantIndices[Random.Range(0, vacantIndices.Count)];
            var mapTile = map[randomIndex];
            mapTile.InitializeTile(BackgroundMapTileType.Lava, randomIndex);
            vacantIndices.Remove(randomIndex);
        }

        return vacantIndices;
    }

    // Returns all map indices excluding winning path indices
    private List<Vector2> GetVacantIndices(List<Vector2> winningPathIndices) {
        var vacantIndices = new List<Vector2>();
        foreach (var tileIndex in map.Keys) {
            // Ignore borders
            if (tileIndex.x == 0 || tileIndex.x == gameModel.GridWidth - 1 ||
                tileIndex.y == 0 || tileIndex.y == gameModel.GridHeight - 1) {
                continue;
            }

            if (!winningPathIndices.Contains(tileIndex)) {
                vacantIndices.Add(tileIndex);
            }
        }

        return vacantIndices;
    }

    // Generates a simple path by calculating distance vector between player and target
    // And progressing towards target in random (horizontal/vertical) direction
    private List<Vector2> GeneratePathToTarget() {
        var distanceToTarget = gameController.TargetInstance.GridIndex - gameController.PlayerInstance.GridIndex;
        var winningPath = new List<Vector2>();
        var currentSimulatedPosition = gameController.PlayerInstance.GridIndex;
        winningPath.Add(currentSimulatedPosition);
        var xProgressSign = distanceToTarget.x > 0 ? 1 : -1;
        var yProgressSign = distanceToTarget.y > 0 ? 1 : -1;
        while (currentSimulatedPosition != gameController.TargetInstance.GridIndex) {
            // Randomly choose direction and verify we haven't passed target in that direction
            var shouldMoveHorizontally = Random.value > 0.5;
            if (shouldMoveHorizontally &&
                !Mathf.Approximately(currentSimulatedPosition.x, gameController.TargetInstance.GridIndex.x)) {
                currentSimulatedPosition += Vector2.right * xProgressSign;
            } else if (!shouldMoveHorizontally &&
                       !Mathf.Approximately(currentSimulatedPosition.y, gameController.TargetInstance.GridIndex.y)) {
                currentSimulatedPosition += Vector2.up * yProgressSign;
            }
            winningPath.Add(currentSimulatedPosition);
        }

        return winningPath;
    }

    // Target position is set on the opposite "quadrant" of the map
    // (Split the map to 4 and place target on diagonal section to players section
    private ForegroundMapTile SpawnTarget(ForegroundMapTile player) {
        var midPoint = new Vector2(gameModel.GridWidth / 2, gameModel.GridHeight / 2);
        // If player to the left of midPoint, choose x to the right
        int randomX = player.GridIndex.x < midPoint.x
            ? Mathf.FloorToInt(Random.Range(midPoint.x, gameModel.GridWidth - 1))
            : Mathf.FloorToInt(Random.Range(1, midPoint.x));
        // If player is below midPoint, choose y above
        var randomY = player.GridIndex.y < midPoint.y
            ? Mathf.FloorToInt(Random.Range(midPoint.y, gameModel.GridHeight - 1))
            : Mathf.FloorToInt(Random.Range(1, midPoint.y));
        var target = InstantiateForegroundMapTile(randomX, randomY, ForegroundMapTileType.Exit, targetPrefab);
        gameController.TargetInstance = target;
        return target;
    }

    private ForegroundMapTile SpawnPlayer() {
        // Ignore wall indices when choosing player index
        int randomX = Random.Range(1, gameModel.GridWidth - 1);
        var randomY = Random.Range(1, gameModel.GridHeight - 1);
        var player = InstantiateForegroundMapTile(randomX, randomY, ForegroundMapTileType.Player, playerPrefab);
        gameController.PlayerInstance = player;
        return player;
    }

    private void SpawnCollectibles(List<Vector2> winningIndices, List<Vector2> vacantIndices) {
        for (var i = 0; i < 3; i++) {
            var randomIndex = winningIndices[Random.Range(0, winningIndices.Count)];
            var collectibleIndex = TryVariance(randomIndex, vacantIndices);
            if (collectibleIndex == randomIndex) {
                winningIndices.Remove(randomIndex);
            } else {
                vacantIndices.Remove(collectibleIndex);
            }
            var collectible = InstantiateForegroundMapTile(collectibleIndex.x, collectibleIndex.y, ForegroundMapTileType.Collectible, collectiblePrefab);
            gameController.CollectibleInstances.Add(collectible);
        }
    }

    // Try placing around the winning path for some variance in the placements
    private Vector2 TryVariance(Vector2 currentIndex, List<Vector2> vacantIndices) {
        // Choose random direction to randomize the checking order
        var xDirection = Random.value > 0.5 ? 1 : -1;
        var yDirection = Random.value > 0.5 ? 1 : -1;
        if (vacantIndices.Contains(currentIndex + new Vector2(xDirection, yDirection))) {
            return currentIndex + new Vector2(xDirection, yDirection);
        }
        if (vacantIndices.Contains(currentIndex + new Vector2(xDirection, -yDirection))) {
            return currentIndex + new Vector2(xDirection, -yDirection);
        }
        if (vacantIndices.Contains(currentIndex + new Vector2(-xDirection, yDirection))) {
            return currentIndex + new Vector2(-xDirection, yDirection);
        }
        if (vacantIndices.Contains(currentIndex + new Vector2(-xDirection, -yDirection))) {
            return currentIndex + new Vector2(-xDirection, -yDirection);
        }

        return currentIndex;
    }

    private ForegroundMapTile InstantiateForegroundMapTile(float x, float y, ForegroundMapTileType foregroundMapTileType, ForegroundMapTile prefab) {
        var interactableIndex = new Vector2(x, y);
        var interactableTile = Instantiate(prefab, interactableIndex, Quaternion.identity, transform);
        interactableTile.InitializeTile(foregroundMapTileType, interactableIndex);
        return interactableTile;
    }

    // Check tile index and choose a tile type accordingly (edges get wall, internal get floor)
    private BackgroundMapTileType GetTileType(Vector2 tileIndex) {
        if (Mathf.Approximately(tileIndex.x, 0) ||
            Mathf.Approximately(tileIndex.x, gameModel.GridWidth - 1) ||
            Mathf.Approximately(tileIndex.y, 0) ||
            Mathf.Approximately(tileIndex.y, gameModel.GridHeight - 1)) {
            return BackgroundMapTileType.Wall;
        }

        return BackgroundMapTileType.Floor;
    }
}
